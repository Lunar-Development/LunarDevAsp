using LunarDev.Data;
using LunarDev.Services;
using LunarDev.Areas.Identity.Data;
using LunarDev.Controllers.Twitch;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.HttpOverrides;
using MySqlConnector;
using Amazon.S3;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;

var connectionString = configuration.GetConnectionString("DefaultConnection");;
var MysqlConnectionString = new MySqlConnectionStringBuilder
{
    Server = configuration.GetConnectionString("Host"),
    Port = Convert.ToUInt16(configuration.GetConnectionString("Port")),
    UserID = configuration.GetConnectionString("User"),
    Password = configuration.GetConnectionString("Password"),
    Database = configuration.GetConnectionString("Database")
};

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

#region Authentication
object value = services.AddAuthentication()
    .AddGoogle(googleOptions =>
    {
        googleOptions.ClientId = configuration["Authentication:Google:ClientId"];
        googleOptions.ClientSecret = configuration["Authentication:Google:ClientSecret"];
    })
    .AddGitHub(options => {
        options.ClientId = configuration["Authentication:Github:ClientId"];
        options.ClientSecret = configuration["Authentication:Github:ClientSecret"];
    })
    .AddDiscord(options => {
        options.ClientId = configuration["Authentication:Discord:ClientId"];
        options.ClientSecret = configuration["Authentication:Discord:ClientSecret"];
    })
    .AddTwitch(options =>
    {
        options.ClientId = configuration["Authentication:Twitch:ClientId"];
        options.ClientSecret = configuration["Authentication:Twitch:ClientSecret"];
    });

services.AddAuthorization(options =>
    {
        options.AddPolicy("StaffAccess", policy =>
            policy.RequireRole("Moderator", "Admin", "SuperAdmin"));
    });
#endregion

#region Services
// Add services to the container.
services.AddDbContext<AuthContext>(options =>
    options.UseMySql(connectionString, new MySqlServerVersion(new Version()))); ;

services.AddDefaultIdentity<AuthUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<AuthContext>()
    .AddDefaultTokenProviders();

services.AddDbContext<AuthContext>(options =>
    options.UseMySql(connectionString, new MySqlServerVersion(new Version()))); ;

services.AddControllersWithViews();

services.AddHttpContextAccessor();

services.AddDatabaseDeveloperPageExceptionFilter();

services.AddTransient<IEmailSender, EmailSender>();

services.AddApiVersioning(o =>
{
    o.AssumeDefaultVersionWhenUnspecified = true;
    o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
    o.ReportApiVersions = true;
    o.ApiVersionReader = ApiVersionReader.Combine(
        new QueryStringApiVersionReader("api-version"),
        new HeaderApiVersionReader("X-Version"),
        new MediaTypeApiVersionReader("ver"));
});

services.AddSingleton<IS3Service, S3Service>()
        .AddAWSService<IAmazonS3>();

services.AddSignalR();
#endregion

#region App
var app = builder.Build();

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseAuthentication();
app.UseAuthorization();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseStaticFiles();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.UseEndpoints(endpoints =>
    {
        endpoints.MapControllers();
        endpoints.MapRazorPages();
    });

#region Websocket

app.MapHub<TwitchNotificationHub>("/twitch/hub");

#endregion

using (var scope = app.Services.CreateScope())
{
    var ScopeServices = scope.ServiceProvider;
    var loggerFactory = ScopeServices.GetRequiredService<ILoggerFactory>();
    try
    {
        var context = ScopeServices.GetRequiredService<AuthContext>();
        var userManager = ScopeServices.GetRequiredService<UserManager<AuthUser>>();
        var roleManager = ScopeServices.GetRequiredService<RoleManager<IdentityRole>>();
        await ContextSeed.SeedRolesAsync(userManager, roleManager);
    }
    catch (Exception ex)
    {
        var logger = loggerFactory.CreateLogger<Program>();
        logger.LogError(ex, "An error occurred seeding the DB.");
    }
}

app.Run();
#endregion