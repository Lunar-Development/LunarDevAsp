﻿namespace LunarDev.Models
{
    public class BlogArticleModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Article { get; set; }
        public byte[]? Thumbnail { get; set; }
        public string Slug { get; set; }
        public long Views { get; set; } = 0;
        public bool IsDraft { get; set; } = true;
        public DateTime PublishDate { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
