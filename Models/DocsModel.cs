﻿namespace LunarDev.Models
{
    public class DocsModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HexColour { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
