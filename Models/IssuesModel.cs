﻿using LunarDev.Areas.Identity.Data;
using LunarDev.Attributes;
using LunarDev.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunarDev.Models
{
    public class ProjectsModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Icon { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public AuthUser User { get; set; }
        public List<IssuesModel> Issues { get; set; }
    }

    public class ProjectsIndexModel
    {
        public IEnumerable<ProjectsModel> projects { get; set; }
    }

    public enum IssueStatus
    {
        [StringValue("To do")]
        Todo,
        [StringValue("In progress")]
        Progress,
        [StringValue("Testing")]
        Testing,
        [StringValue("Solved")]
        Solved,
        [StringValue("Cancelled")]
        Canceled
    }

    public class IssuesModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Issue { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public IssueStatus Status { get; set; }

        public int ProjectId { get; set; }
        [NotMapped]
        public ProjectsModel Project { get; set; }

        
        public List<SelectListItem> StatusTypes()
        {
            return new List<SelectListItem>
            {
               new SelectListItem(IssueStatus.Todo.ToString(), ((int)IssueStatus.Todo).ToString()),
               new SelectListItem(IssueStatus.Progress.ToString(), ((int)IssueStatus.Progress).ToString()),
               new SelectListItem(IssueStatus.Testing.ToString(), ((int)IssueStatus.Testing).ToString()),
               new SelectListItem(IssueStatus.Solved.ToString(), ((int)IssueStatus.Solved).ToString()),
               new SelectListItem(IssueStatus.Canceled.ToString(), ((int)IssueStatus.Canceled).ToString())
            };
        }
    }
}
