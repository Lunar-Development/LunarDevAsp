using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LunarDev.Models
{
    public class ResponseModel
    {
        public int Code { get; set;}
        public string Message { get; set; } = "Message Not Loaded";
        public Hashtable? Data { get; set; }
    }

    public class TwtichStateListModel
    {
        public string Code { get; set; }
    }

    public class SettingModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}