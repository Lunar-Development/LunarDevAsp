﻿using LunarDev.Areas.Identity.Data;

namespace LunarDev.Models
{
    public class LinksModel
    {
        public string Slug { get; set; }
        public string Url { get; set; }

        public string UserId { get; set; }
        public AuthUser User { get; set; }
    }
}
