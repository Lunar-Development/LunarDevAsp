﻿namespace LunarDev.Models.Twitch
{
    public class ChannelPointModels
    {
        public int Id { get; set; }
        public long BroadcasterId { get; set; }
        public long RewardId { get; set; }
        public string ImageUrl { get; set; }
        public string AudioUrl { get; set; }
        public string Text { get; set; } = "{user} redeemed ${rewardTitle}";
    }

    public class ChannelPointDefaultModel
    {
        public long Id { get; set; }
        public long BroadcasterId { get; set; }
        public string ImageUrl { get; set; }
        public string AudioUrl { get; set; }
        public string Text { get; set; } = "{user} redeemed {rewardTitle}";
    }

    public class ChannelPointCreateModel
    {
        public long BroadcasterId { get; set; }
        public byte[] Image { get; set; }
        public byte[] Audio { get; set; }
        public string Text { get; set; }
        public bool IsDefault { get; set; }
    }

}
