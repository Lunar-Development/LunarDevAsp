﻿window.addEventListener("load", () => {
    const uri = document.getElementById("qrCodeData").getAttribute('data-url');
    new QRCode(document.getElementById("qrCode"),
        {
            text: uri,
            width: 150,
            height: 150,
        });
    document.querySelector("#qrCode>img").classList.add("bg-light", "p-2", "mb-2")
});