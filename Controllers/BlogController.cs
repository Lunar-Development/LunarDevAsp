﻿using LunarDev.Areas.Identity.Data;
using LunarDev.Data;
using LunarDev.Models;
using LunarDev;
using Markdig;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Html2Markdown;

namespace LunarDev.Controllers
{
    [Route("blog")]
    public class BlogController : Controller
    {
        private ILogger<BlogController> _logger;
        private AuthContext _authContext;
        private UserManager<AuthUser> _userManager;

        public BlogController(ILogger<BlogController> logger, AuthContext authContext, UserManager<AuthUser> userManager)
        {
            _logger = logger;
            _authContext = authContext;
            _userManager = userManager;
        }

        [Route("")]
        public IActionResult Index(int? page)
        {
            var date = DateTime.UtcNow;
            var blogs = _authContext.Blogs.Where(b =>
                (b.IsDraft == false) && (DateTime.Compare(b.PublishDate, date) <= 0))
                .ToList(); ;

            if (page == null)
            {
                page = 1;
            }

            int pageSize = 8;
            int pageNumber = (page ?? 1);

            return View(PaginatedList<BlogArticleModel>.Create(blogs, pageNumber, pageSize));
        }

        [Route("article/{slug}")]
        public async Task<IActionResult> ViewArticleAsync(string slug)
        {
            var article = _authContext.Blogs.FirstOrDefault(b => b.Slug == slug);

            article.Views++;
            _authContext.Blogs.Update(article);
            await _authContext.SaveChangesAsync();

            return View(article);
        }

        [Authorize(Policy = "StaffAccess")]
        [HttpGet]
        [Route("Admin/CreateArticle")]
        public IActionResult CreateArticle()
        {
            return View(new BlogArticleModel());
        }

        [Authorize(Policy = "StaffAccess")]
        [HttpPost]
        [Route("Admin/CreateArticle")]
        public async Task<IActionResult> CreateArticle(BlogArticleModel model)
        {
            BlogArticleModel article = new BlogArticleModel();
            if (ModelState.IsValid)
            {
                IFormFile file = Request.Form.Files.FirstOrDefault();
                using (var dataStream = new MemoryStream())
                {
                    await file.CopyToAsync(dataStream);
                    article.Thumbnail = dataStream.ToArray();
                }

                string htmlArticle = Markdown.ToHtml(model.Article);

                article.Title = model.Title;
                article.PublishDate = model.PublishDate;
                article.Article = htmlArticle;
                article.Description = model.Description;
                article.IsDraft = model.IsDraft;
                article.Slug = model.Slug;

                _authContext.Blogs.Add(article);
                await _authContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize(Policy = "StaffAccess")]
        [HttpGet]
        [Route("Admin/EditArticle")]
        public IActionResult EditArticle(int id)
        {
            var article = _authContext.Blogs.FirstOrDefault(b => b.Id == id);

            var converter = new Converter();
            article.Article = converter.Convert(article.Article);

            return View(article);
        }

        [Authorize(Policy = "StaffAccess")]
        [HttpPost]
        [Route("Admin/EditArticle")]
        public async Task<IActionResult> EditArticle(int id, BlogArticleModel model)
        {
            var article = await _authContext.Blogs.FirstOrDefaultAsync(b => b.Id == id);
            if (ModelState.IsValid)
            {
                if (Request.Form.Files.Count > 0)
                {
                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        await file.CopyToAsync(dataStream);
                        article.Thumbnail = dataStream.ToArray();
                    }
                }

                string htmlArticle = Markdown.ToHtml(model.Article);
                article.Article = htmlArticle;
                article.Title = model.Title;
                article.Description = model.Description;
                article.IsDraft = model.IsDraft;
                article.PublishDate = model.PublishDate;

                _authContext.Blogs.Update(article);
                await _authContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(model);
        }
    }
}
