﻿using LunarDev.Areas.Identity.Data;
using LunarDev.Data;
using LunarDev.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LunarDev.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class UserRolesController : Controller
    {
        private readonly UserManager<AuthUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly AuthContext _authContext;
        public UserRolesController(UserManager<AuthUser> userManager, RoleManager<IdentityRole> roleManager, AuthContext authContext)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _authContext = authContext;
        }
        public async Task<IActionResult> Index()
        {
            var users = _userManager.Users.ToList();
            var userRolesViewModel = new List<UserRolesModel>();
            foreach (AuthUser user in users)
            {
                var thisViewModel = new UserRolesModel();
                thisViewModel.UserId = user.Id;
                thisViewModel.Email = user.Email;
                thisViewModel.FirstName = user.FirstName;
                thisViewModel.LastName = user.LastName;
                thisViewModel.Roles = await GetUserRoles(user);
                userRolesViewModel.Add(thisViewModel);
            }
            return View(userRolesViewModel);
        }
        private async Task<List<string>> GetUserRoles(AuthUser user)
        {
            return new List<string>(await _userManager.GetRolesAsync(user));
        }

        public async Task<IActionResult> Manage(string userId)
        {
            ViewBag.userId = userId;
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                ViewBag.ErrorMessage = $"User with Id = {userId} cannot be found";
                return View("NotFound");
            }
            ViewBag.UserName = user.UserName;
            var model = new List<ManageUserRolesViewModel>();
            var roles = _roleManager.Roles;
            foreach (var role in roles.ToList())
            {
                var userRolesViewModel = new ManageUserRolesViewModel
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    userRolesViewModel.Selected = true;
                }
                else
                {
                    userRolesViewModel.Selected = false;
                }
                model.Add(userRolesViewModel);
                
            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Manage(List<ManageUserRolesViewModel> model, string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return View();
            }
            var roles = await _userManager.GetRolesAsync(user);
            var result = await _userManager.RemoveFromRolesAsync(user, roles);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot remove user existing roles");
                return View(model);
            }
            result = await _userManager.AddToRolesAsync(user, model.Where(x => x.Selected).Select(y => y.RoleName));
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Cannot add selected roles to user");
                return View(model);
            }
            return RedirectToAction("Index");
        }
    }

    [Route("UserActions")]
    public class UserActionController : Controller
    {
        private readonly UserManager<AuthUser> _userManager;
        private readonly AuthContext _authContext;
        public UserActionController(AuthContext authContext, UserManager<AuthUser> userManager)
        {
            _authContext = authContext;
            _userManager = userManager;
        }

        public string GenerateToken()
        {
            Random rnd = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!/#][;:>.<,\"£$%^&*()-_=+'";
            var randomString = new string(Enumerable.Repeat(chars, rnd.Next(24, 48))
                        .Select(s => s[rnd.Next(s.Length)]).ToArray());
            return randomString;
        }

        [HttpPost]
        [Authorize]
        [Route("UserApiTokenReset")]
        public async Task<IActionResult> ResetApiToken()
        {
            var randomString = GenerateToken();
            var users = _authContext.Users.Where(x => x.AuthCode == randomString).ToList();

            while (users.Count > 0)
            {
                randomString = GenerateToken(); 
                users = _authContext.Users.Where(x => x.AuthCode == randomString).ToList();
            }

            var user = await _userManager.GetUserAsync(User);
            user.AuthCode = randomString;
            _authContext.SaveChanges();

            return Redirect("/Identity/Account/Manage/Index");
        }
    }
}
