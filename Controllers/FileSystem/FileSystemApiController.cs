﻿using LunarDev.Services;
using Microsoft.AspNetCore.Mvc;

namespace LunarDev.Controllers.FileSystem
{
    [Route("api/v{version:apiVersion}/FileSystem")]
    [ApiVersion("1.1")]
    [ApiController]
    public class FileSystemApiController : Controller
    {
        private readonly S3Service _s3Service;
        public FileSystemApiController(S3Service s3Service)
        {
            _s3Service = s3Service;
        }

        [Route("add")]
        public async Task<string> AddFileAsync([FromForm] string path, [FromForm] string fileName)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));

            var url = string.Empty;

            IFormFile file = Request.Form.Files.FirstOrDefault();
            using (var dataStream = new MemoryStream())
            {
                await file.CopyToAsync(dataStream);
                url = await _s3Service.UploadFileAsync(dataStream, path, fileName);
            }

            return url;
        }

        [Route("remove")]
        public async Task<bool> RemoveFileAsync([FromForm] string path, [FromForm] string fileName)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            if (fileName == null) throw new ArgumentNullException(nameof(fileName));
            
            try
            {
                await _s3Service.DeleteFileAsync(path, fileName);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }

        }

    }
}
