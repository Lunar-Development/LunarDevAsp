﻿using LunarDev.Areas.Identity.Data;
using LunarDev.Data;
using LunarDev.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LunarDev.Controllers
{
    [Route("")]
    [Authorize]
    public class IssuesController : Controller
    {
        private readonly ILogger _logger;
        private readonly UserManager<AuthUser> _userManager;
        private readonly AuthContext _authContext;

        public IssuesController(ILogger<IssuesController> logger, UserManager<AuthUser> userManager, AuthContext authContext)
        {
            _logger = logger;
            _userManager = userManager;
            _authContext = authContext;
        }

        [Route("tracker")]
        public async Task<ActionResult> IndexAsync()
        {
            AuthUser user = await _userManager.GetUserAsync(User);

            var projects = new ProjectsIndexModel()
                .projects = _authContext.Projects.Include(x => x.Issues).Where(x => x.User.Id == user.Id).ToList();

            return View(projects);
        }

        [Route("projects/create")]
        [HttpPost]
        public async Task<ActionResult> ProjectCreate(ProjectsModel model)
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                model.UserId = user.Id;

                if (Request.Form.Files.Count > 0)
                {
                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        await file.CopyToAsync(dataStream);
                        model.Icon = dataStream.ToArray();
                    }
                }

                _authContext.Projects.Add(model);
                _authContext.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ErrorViewModel error = new ErrorViewModel();
                return View("Error", error);
            }
        }

        [Route("projects/edit")]
        [HttpPost]
        public async Task<ActionResult> ProjectEdit(ProjectsModel model)
        {
            var project = await _authContext.Projects.FindAsync(model.Id);
            try
            {
                if (Request.Form.Files.Count > 0)
                {
                    IFormFile file = Request.Form.Files.FirstOrDefault();
                    using (var dataStream = new MemoryStream())
                    {
                        await file.CopyToAsync(dataStream);
                        project.Icon = dataStream.ToArray();
                    }
                }

                project.Title = model.Title;
                project.Description = model.Description;

                _authContext.Projects.Update(project);
                _authContext.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                var error = new ErrorViewModel()
                    .RequestId = "500";
                return RedirectToAction("Index");
            }
        }

        [Route("projects/delete")]
        [HttpPost]
        public async Task<ActionResult> ProjectDelete(int id)
        {
            var model = _authContext.Projects.Find(id);
            var curUserId = (await _userManager.GetUserAsync(User)).Id;
            if (model.User.Id == curUserId)
            {
                _authContext.Projects.Remove(model);
                _authContext.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [Route("issues/")]
        [HttpGet]
        public ActionResult IssueList(int projectId)
        {
            if (projectId == 0) return RedirectToAction("Index");

            var issueModel = _authContext.Issues
                .Include(x => x.Project)
                .Where(x => x.ProjectId == projectId).ToList();

            return View(issueModel);
        }

        [Route("issues/edit")]
        [HttpPost]
        public async Task<ActionResult> IssueEdit(IssuesModel model)
        {
            try
            {
                _authContext.Issues.Update(model);
                _authContext.SaveChanges();
                return RedirectToAction("IssueList", new { projectId=model.ProjectId});
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View("Error", new ErrorViewModel());
            }
        }

        [Route("issues/boardChange")]
        [HttpPost]
        public async Task<ActionResult> IssueBoardChange(int Id, string Board)
        {
            try
            {
                var model = await _authContext.Issues.FindAsync(Id);
                switch (Board) {
                    case "_todo":
                        model.Status = IssueStatus.Todo;
                        break;
                    case "_working":
                        model.Status = IssueStatus.Progress;
                        break;
                    case "_solved":
                        model.Status = IssueStatus.Solved;
                        break;
                    case "_canceled":
                        model.Status = IssueStatus.Canceled;
                        break;
                }
                _authContext.Issues.Update(model);
                _authContext.SaveChanges();
                return RedirectToAction("IssueList", new { projectId=model.ProjectId});
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View("Error", new ErrorViewModel());
            }
        }

        [Route("issues/create")]
        [HttpPost]
        public async Task<ActionResult> IssueCreate(IssuesModel model)
        {
            try
            {
                model.Status = IssueStatus.Todo;
                _authContext.Issues.Add(model);
                _authContext.SaveChanges();
                return RedirectToAction("IssueList", new { projectId = model.ProjectId });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return View("Error", new ErrorViewModel());
            }
        }

        [Route("issues/delete")]
        public async Task<ActionResult> IssueDelete(int id)
        {
            var issue = _authContext.Issues
                .Include(x => x.Project)
                .Where(x => x.Id == id)
                .ToList().First();

            var projectId = issue.ProjectId;
            var project = _authContext.Projects
                .Include(x => x.User)
                .FirstOrDefault(x => x.Id == projectId);

            var curUserId = (await _userManager.GetUserAsync(User)).Id;
            if (project.User.Id == curUserId)
            {
                _authContext.Issues.Remove(issue);
                _authContext.SaveChanges();
            }

            return RedirectToAction("IssueList", new { projectId = projectId });
        }
    }
}
