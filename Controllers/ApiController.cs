using System.Collections;
using LunarDev.Models;
using LunarDev.Areas.Identity.Data;
using LunarDev.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LunarDev.Controllers
{
    [Route("api/v{version:apiVersion}")]
    [ApiVersion("1.0")]
    [ApiController]
    public class DataController : Controller
    {
        private UserManager<AuthUser> _userManager;
        private AuthContext _authContext;

        public DataController(UserManager<AuthUser>? userManager,
                              AuthContext? authContext)
        {
            _authContext = authContext;
            _userManager = userManager;
        }

        [Route("ping")]
        [HttpGet]
        public ResponseModel PingPong()
        {
            var response = new ResponseModel();
            response.Code = 200;
            response.Message = "Pong! Message received";
            return response;
        }

        [Route("CreateAuthCode")]
        [HttpGet]
        public ResponseModel CreateAuthCode()
        {
            Random rnd = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!/#][;:>.<,\"�$%^&*()-_=+'";
            var randomString = new string(Enumerable.Repeat(chars, rnd.Next(24, 48))
                        .Select(s => s[rnd.Next(s.Length)]).ToArray());

            var response = new ResponseModel();
            response.Code = 200;
            response.Message = "Auth Code Created";
            response.Data = new Hashtable();
            response.Data.Add("AuthCode", randomString);
            return response;
        }

        [Route("GetIssue/Stats")]
        [HttpGet]
        public ResponseModel GetIssueStats()
        {
            try 
            {
                var Issues = _authContext.Issues.ToList();
                var response = new ResponseModel();


                response.Code = 200;
                response.Message = "Data has been collected";
                response.Data = new Hashtable();
                response.Data.Add("count", Issues.Count());
                response.Data.Add("issues", Issues);
                return response;
            }
            catch
            {
                var response = new ResponseModel();
                response.Code = 500;
                response.Message = "An error has occurred";
                return response;
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("Error!");
        }
    }
}