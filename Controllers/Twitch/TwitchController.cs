﻿using LunarDev.Models;
using LunarDev.Areas.Identity.Data;
using LunarDev.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System.Collections;
using System.Net.Http.Headers;
using System.Text;

namespace LunarDev.Controllers.Twitch
{
    [Route("twitch")]
    public class TwitchController : Controller
    {
        private AuthContext _context { get; set; }
        private UserManager<AuthUser> _userManager { get; set; }
        private IHubContext<TwitchNotificationHub> _hubContext { get; set; }
        private readonly ILogger _logger;
        public List<string> stateList { get; set; }

        public TwitchController(
            AuthContext context,
            UserManager<AuthUser> userManager,
            IHubContext<TwitchNotificationHub> hubContext,
            ILogger<TwitchController> logger)
        {
            _context = context;
            _userManager = userManager;
            stateList = new List<string>();
            _hubContext = hubContext;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Route("getaccesstoken")]
        public async Task<ResponseModel> GetAccessTokenAsync()
        {
            ResponseModel response = new ResponseModel();
            response.Code = 500;
            response.Message = "Server Error, contact server admin";

            try
            {
                HttpClient client = new HttpClient();
                var clientId = _context.Settings.Find("twitch_api_client_id").Value;
                var clientSecret = _context.Settings.Find("twitch_api_client_secret").Value;
                Dictionary<string, string> formData = new Dictionary<string, string>();
                formData.Add("client_id", clientId);
                formData.Add("client_secret", clientSecret);
                formData.Add("grant_type", "client_credentials");

                client.DefaultRequestHeaders.Accept.Clear();

                var url = "https://id.twitch.tv/oauth2/token";
                var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(formData) };
                var res = await client.SendAsync(req);
                var data = await res.Content.ReadFromJsonAsync<Hashtable>();

                response.Code = 200;
                response.Message = "Token Generated";
                response.Data = data;

                var accessToken = _context.Settings.Find("twitch_api_access_token");
                accessToken.Value = data["access_token"].ToString();

                _context.Settings.Update(accessToken);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return response;
        }

        [Route("CreateSubscription/{userId}")]
        public async Task<ResponseModel> CreateSubscription(string userId)
        {
            ResponseModel response = new ResponseModel();
            response.Code = 500;
            response.Message = "Server Error, contact server admin";

            try
            {
                HttpClient client = new HttpClient();
                var accessToken = _context.Settings.Find("twitch_api_access_token").Value;
                var clientId = _context.Settings.Find("twitch_api_client_id").Value;
                var signature = _context.Settings.Find("twitch_api_signature").Value;

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                client.DefaultRequestHeaders.Add("Client-Id", clientId);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var formData = new Hashtable();
                formData.Add("type", "channel.channel_points_custom_reward_redemption.add");
                formData.Add("version", "1");
                
                var condition = new Dictionary<string, string>();
                condition.Add("broadcaster_user_id", userId);
                formData.Add("condition", condition);

                var transport = new Dictionary<string, string>();
                transport.Add("method", "webhook");
                transport.Add("callback", "https://lunar-dev.com/twitch/listeners/channel_points");
                transport.Add("secret", signature);
                formData.Add("transport", transport);

                var uri = "https://api.twitch.tv/helix/eventsub/subscriptions";

                var postRequest = new HttpRequestMessage(HttpMethod.Post, uri)
                {
                    Content = JsonContent.Create(formData)
                };

                var res = await client.SendAsync(postRequest);
                var data = await res.Content.ReadFromJsonAsync<Hashtable>();

                response.Code = 200;
                response.Message = "Token Generated";
                response.Data = data;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return response;
        }

        [Route("listeners/channel_points")]
        public async Task<IActionResult> ChannelPointListeners([FromBody]Hashtable body)
        {
            Console.WriteLine("Twitch Channel Points | Listener Triggered");
            const string MESSAGE_TYPE = "Twitch-Eventsub-Message-Type";
            const string MESSAGE_VERFICATION_HEADER = "webhook_callback_verification";
            const string MESSAGE_NOTIFICATION = "notification";

            if (Request.Headers.ContainsKey(MESSAGE_TYPE))
            {
                var messageType = Request.Headers[MESSAGE_TYPE];
                Console.WriteLine(messageType);
                if (messageType == MESSAGE_VERFICATION_HEADER)
                {
                    return Ok(body["challenge"].ToString());
                }
                else if (messageType == MESSAGE_NOTIFICATION)
                {
                    var eventData = JsonConvert.DeserializeObject<Hashtable>(body["event"].ToString());
                    var broadcasterId = eventData["broadcaster_user_id"].ToString();
                    var userName = eventData["user_name"].ToString();

                    var reward = JsonConvert.DeserializeObject<Hashtable>(eventData["reward"].ToString());
                    var rewardId = reward["id"];
                    var rewardTitle = reward["title"];

                    await _hubContext.Clients.Group(broadcasterId).SendAsync("ReceiveMessage", userName, rewardId, rewardTitle);
                    return Ok(0);
                }

                return NotFound();
            }
                return NotFound();


        }

        [Route("listeners/test")]
        public IActionResult Testing()
        {
            Console.WriteLine("Twitch Channel Points | Page Visited");
            return View("Listeners/ChannelPoints");
        }

        #region Authorize App For twitch usage


        [Route("AuthorizeApp")]
        public async Task<IActionResult> AuthorizeTwitchFeaturesAsync()
        {
            ResponseModel response = new ResponseModel();
            response.Code = 500;
            response.Message = "Server Error, contact server admin";

            try
            {
                var scopes = "channel:read:redemptions";
                var responseCode = "code";
                var clientId = _context.Settings.Find("twitch_api_client_id").Value;

                Random rnd = new Random();
                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var stateString = new string(Enumerable.Repeat(chars, rnd.Next(32, 128))
                            .Select(s => s[rnd.Next(s.Length)]).ToArray());

                TwtichStateListModel twichStateModel = new TwtichStateListModel();
                twichStateModel.Code = stateString;
                _context.TwitchStateList.Add(twichStateModel);
                await _context.SaveChangesAsync();

                var redirectUri = Url.ActionLink("AuthRedirect");
                
                var uri = $"https://id.twitch.tv/oauth2/authorize?" +
                    $"response_type={responseCode}&" +
                    $"client_id={clientId}&" +
                    $"redirect_uri={redirectUri}&" +
                    $"scope={scopes}&" +
                    $"state={stateString}";
                return Redirect(uri);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return RedirectToAction("Index", "Home");
        }

        [Route("redirect")]
        public async Task<ResponseModel> AuthRedirect(
            string? code,
            string? scope, 
            string? error,
            string? error_description,
            string state
            )
        {
            ResponseModel response = new ResponseModel();
            response.Code = 500;
            response.Message = "Server Error, contact server admin";

            try
            {
                var stateModel = _context.TwitchStateList.Find(state);
                if (stateModel == null)
                {
                    response.Code = 403;
                    response.Message = "Unauthorized Access";
                    return response;
                }
                if (error != null)
                {
                    response.Code = 400;
                    response.Message = error_description;
                    return response;
                }

                _context.TwitchStateList.Remove(stateModel);

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();

                var clientId = _context.Settings.Find("twitch_api_client_id").Value;
                var clientSecret = _context.Settings.Find("twitch_api_client_secret").Value;
                var grantType = "authorization_code";
                var redirectUri = Url.ActionLink("AuthRedirect");

                var uri = $"https://id.twitch.tv/oauth2/token?" +
                    $"client_id={clientId}&" +
                    $"client_secret={clientSecret}&" +
                    $"code={code}&" +
                    $"grant_type={grantType}&" +
                    $"redirect_uri={redirectUri}";

                var req = new HttpRequestMessage(HttpMethod.Post, uri);

                var res = await client.SendAsync(req);
                var data = await res.Content.ReadAsStringAsync();
                Console.WriteLine(data);
                var jsonData = JsonConvert.DeserializeObject<Hashtable>(data);

                var accessToken = jsonData["access_token"].ToString();
                var refreshToken = jsonData["refresh_token"].ToString();

                var user = await _userManager.GetUserAsync(User);
                user.TwitchUserAuthCode = accessToken;
                user.TwitchRefreshToken = refreshToken;
                await _userManager.UpdateAsync(user);

                response.Code = 200;
                response.Message = "User Authorized";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return response;
        }
        #endregion
    }
}
