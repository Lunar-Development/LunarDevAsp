﻿using Microsoft.AspNetCore.SignalR;

namespace LunarDev.Controllers.Twitch
{
    public interface INotificationClient
    {
        Task ReceiveMessage(string message);
    }

    public class TwitchNotificationHub : Hub<INotificationClient>
    {
        public override async Task OnConnectedAsync()
        {
            var userId = Context.GetHttpContext().Request.Query["user_id"];
            Console.WriteLine("Twitch Channel Points | Webhook Connected");
            await Groups.AddToGroupAsync(Context.ConnectionId, userId);
            await base.OnConnectedAsync();
        }

        public async Task SendMessage(string userid, string message)
        {
            await Clients.Groups(userid).ReceiveMessage(message);
        }
    }
}
