﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LunarDev.Data;
using LunarDev.Models;

namespace LunarDev.Controllers
{
    public class DocsController : Controller
    {
        private readonly AuthContext _context;

        public DocsController(AuthContext context)
        {
            _context = context;
        }

        // GET: Docs
        public async Task<IActionResult> Index()
        {
            return View(await _context.DocsModel.ToListAsync());
        }

        // GET: Docs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var docsModel = await _context.DocsModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (docsModel == null)
            {
                return NotFound();
            }

            return View(docsModel);
        }

        // GET: Docs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Docs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,HexColour,Content,CreatedOn")] DocsModel docsModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(docsModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(docsModel);
        }

        // GET: Docs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var docsModel = await _context.DocsModel.FindAsync(id);
            if (docsModel == null)
            {
                return NotFound();
            }
            return View(docsModel);
        }

        // POST: Docs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,HexColour,Content,CreatedOn")] DocsModel docsModel)
        {
            if (id != docsModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(docsModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DocsModelExists(docsModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(docsModel);
        }

        // GET: Docs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var docsModel = await _context.DocsModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (docsModel == null)
            {
                return NotFound();
            }

            return View(docsModel);
        }

        // POST: Docs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var docsModel = await _context.DocsModel.FindAsync(id);
            _context.DocsModel.Remove(docsModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DocsModelExists(int id)
        {
            return _context.DocsModel.Any(e => e.Id == id);
        }
    }
}
