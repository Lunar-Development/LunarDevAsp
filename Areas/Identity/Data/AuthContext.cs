﻿using LunarDev.Models;
using LunarDev.Areas.Identity.Data;
using LunarDev.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace LunarDev.Data;

public class AuthContext : IdentityDbContext<AuthUser>
{
    public AuthContext(DbContextOptions<AuthContext> options)
        : base(options)
    {
    }

    public DbSet<IssuesModel> Issues { get; set; }
    public DbSet<ProjectsModel> Projects { get; set; }
    public DbSet<BlogArticleModel> Blogs { get; set; }

    public DbSet<SettingModel> Settings { get; set; }
    public DbSet<TwtichStateListModel> TwitchStateList { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<IssuesModel>().ToTable("Issues");
        modelBuilder.Entity<ProjectsModel>().ToTable("Projects");
        modelBuilder.Entity<BlogArticleModel>().ToTable("BlogArticles");
        modelBuilder.Entity<SettingModel>().ToTable("Settings");
        modelBuilder.Entity<TwtichStateListModel>().ToTable("TwtichStateList");

        modelBuilder.Entity<ProjectsModel>()
            .HasOne(u => u.User)
            .WithMany(p => p.Projects)
            .HasForeignKey(p => p.UserId)
            .IsRequired();

        modelBuilder.Entity<IssuesModel>()
            .HasOne(i => i.Project)
            .WithMany(p => p.Issues)
            .HasForeignKey(i => i.ProjectId)
            .IsRequired();

        modelBuilder.Entity<SettingModel>()
            .HasKey(x => x.Key);
        
        modelBuilder.Entity<TwtichStateListModel>()
            .HasKey(x => x.Code);

    }

    public DbSet<LunarDev.Models.DocsModel> DocsModel { get; set; }
}
