﻿using LunarDev.Controllers;
using LunarDev.Models;
using Microsoft.AspNetCore.Identity;

namespace LunarDev.Areas.Identity.Data;

// Add profile data for application users by adding properties to the AuthUser class
public class AuthUser : IdentityUser
{
    [PersonalData]
    public string? FirstName { get; set; }

    [PersonalData]
    public string? LastName { get; set; }
    public int UsernameChangeLimit { get; set; } = 10;
    public byte[]? ProfilePicture { get; set; }
    private string authcode = GenerateAuthCode();
    public string AuthCode
    {
        get => authcode;
        set => authcode = value;
    }
    public DateTime CreatedDate { get; set; } = DateTime.Now;
    public bool TwitchAuthorised { get; set; } = false;
    public string? TwitchUserAuthCode { get; set; }
    public string? TwitchRefreshToken { get; set; }
    public DateTime TwitchAuthExpireDate { get; set; } = DateTime.UtcNow;

    public List<ProjectsModel> Projects { get; set; }

    public static string GenerateAuthCode()
    {
        Random rnd = new Random();

        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!/#][;:>.<,\"£$%^&*()-_=+'";
        var randomString = new string(Enumerable.Repeat(chars, rnd.Next(24, 48))
                    .Select(s => s[rnd.Next(s.Length)]).ToArray());
        return randomString;
    }
}

public enum Roles
{
    SuperAdmin,
    Admin,
    Moderator,
    Basic
}