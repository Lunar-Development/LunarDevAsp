﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3.Util;
using LunarDev.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Net.Http.Headers;

namespace LunarDev.Services
{
    public class S3Service: IS3Service
    {
        public string bucketName = "lunardev";
        public string endpointUrl = "https://ams3.digitaloceanspaces.com";
        public IAmazonS3 _client;

        public S3Service()
        {
            var clientConfig = new AmazonS3Config
            {
                ServiceURL = endpointUrl,
            };

            _client = new AmazonS3Client(clientConfig);
        }

        public async Task<string> UploadFileAsync(Stream localFilePath, string subDirectoryInBucket, string fileNameInS3)
        {
            try
            {
                TransferUtility fileTransferUtitlies = new TransferUtility(_client);
                TransferUtilityUploadRequest uploadRequest = new TransferUtilityUploadRequest();

                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    uploadRequest.BucketName = bucketName; //no subdirectory just base bucket directory, try avoid using.
                }
                else
                {   // subdirectory and bucket name  
                    uploadRequest.BucketName = bucketName + @"/" + subDirectoryInBucket;
                }

                uploadRequest.Key = fileNameInS3;
                uploadRequest.InputStream = localFilePath;
                await fileTransferUtitlies.UploadAsync(uploadRequest);

                var url = "https://cdn.lunar-dev.com" + subDirectoryInBucket + fileNameInS3;

                return url;

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);

                return string.Empty;
            }
        }

        public async Task DeleteFileAsync(string subDirectoryInBucket, string fileNameInS3)
        {
            try
            {
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();

                if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
                {
                    deleteRequest.BucketName = bucketName; //no subdirectory just base bucket directory, try avoid using.
                }
                else
                {   // subdirectory and bucket name  
                    deleteRequest.BucketName = bucketName + @"/" + subDirectoryInBucket;
                }
                deleteRequest.Key = fileNameInS3;

                if (IsFileExists(subDirectoryInBucket, fileNameInS3))
                {
                    await _client.DeleteObjectAsync(deleteRequest);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }
        }

        public bool IsFileExists(string subDirectoryInBucket, string fileName)
        {
            try
            {
                GetObjectMetadataRequest request = new GetObjectMetadataRequest()
                {
                    BucketName = bucketName + @"/" + subDirectoryInBucket,
                    Key = fileName
                };

                var response = _client.GetObjectMetadataAsync(request).Result;

                return true;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException is AmazonS3Exception awsEx)
                {
                    if (string.Equals(awsEx.ErrorCode, "NoSuchBucket"))
                        return false;

                    else if (string.Equals(awsEx.ErrorCode, "NotFound"))
                        return false;
                }

                throw;
            }
        }

        public static string GetStaticFileUrl(string filePathInBucket)
        {
            string Url = "";
            string bucketName = "lunardev";
            string endpointUrl = "https://ams3.digitaloceanspaces.com";

            var clientConfig = new AmazonS3Config
            {
                ServiceURL = endpointUrl,
            };

            var _client = new AmazonS3Client(clientConfig);

            try
            {
                var request = new GetPreSignedUrlRequest()
                {
                    BucketName = bucketName,
                    Key = "aspenWeb/static/"+filePathInBucket,
                    Verb = HttpVerb.GET,
                    Expires = DateTime.UtcNow.AddMinutes(10)
                };

                Url = _client.GetPreSignedURL(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }

            return Url;
        }

        public static string GetPrivateMediaFileUrl(string filePathInBucket)
        {
            string Url = "";
            string bucketName = "lunardev";
            string endpointUrl = "https://ams3.digitaloceanspaces.com";

            var clientConfig = new AmazonS3Config
            {
                ServiceURL = endpointUrl,
            };

            var _client = new AmazonS3Client(clientConfig);

            try
            {
                var request = new GetPreSignedUrlRequest()
                {
                    BucketName = bucketName,
                    Key = "aspenWeb/media/private/" + filePathInBucket,
                    Verb = HttpVerb.GET,
                    Expires = DateTime.UtcNow.AddMinutes(10)
                };

                Url = _client.GetPreSignedURL(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }

            return Url;
        }

        public static string GetPublicMediaFileUrl(string filePathInBucket)
        {
            string Url = "";
            string bucketName = "lunardev";
            string endpointUrl = "https://ams3.digitaloceanspaces.com";

            var clientConfig = new AmazonS3Config
            {
                ServiceURL = endpointUrl,
            };

            var _client = new AmazonS3Client(clientConfig);

            try
            {
                var request = new GetPreSignedUrlRequest()
                {
                    BucketName = bucketName,
                    Key = "aspenWeb/media/public/" + filePathInBucket,
                    Verb = HttpVerb.GET,
                    Expires = DateTime.UtcNow.AddMinutes(10)
                };

                Url = _client.GetPreSignedURL(request);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
            }

            return Url;
        }

    }
}
