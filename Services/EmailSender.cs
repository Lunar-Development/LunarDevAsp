﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System.Net;
using System.Net.Mail;

namespace LunarDev.Services
{
    public class EmailSender : IEmailSender
    {
        string MailID, AppPassword;
        public EmailSender(IConfiguration configuration)
        {
            MailID = configuration["Email:MailID"];
            AppPassword = configuration["Email:AppPassword"];
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var Body = "<html>"+
                      "<body>" +
                      "<div style = \"text-align:center\">" +
                      $"<h1> {subject} </h1>" +
                      "<h3> Lunar Development</h3>" +
                      "<hr/>" +
                      "</div>" +
                      "<div style = \"margin: 1rem 0.5rem 2rem;\">" +
                      htmlMessage +
                      "</div>" +
                      "<footer style = \"text -align:center\">" +
                      "<hr />" +
                      "This is just a temporary email template this wil be changing in the near future" +
                      "</footer>" +
                      "</body> "+
                      "</html>";
            MailMessage message = new MailMessage();
            message.From = new MailAddress(MailID);
            message.Subject = subject;
            message.To.Add(new MailAddress(email));
            message.Body = Body;
            message.IsBodyHtml = true;

            var smtpClient = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials = new NetworkCredential(MailID, AppPassword),
                EnableSsl = true,
            };
            smtpClient.Send(message);
            return Task.CompletedTask;
        }
    }
}
