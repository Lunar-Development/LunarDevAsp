﻿using Microsoft.EntityFrameworkCore;

namespace LunarDev
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }

        public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            AddRange(items);
        }

        public bool HasPreviousPage => PageIndex > 1;

        public bool HasNextPage => PageIndex < TotalPages;

        public static PaginatedList<T> Create(List<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }
    }

    public class WebhookOptions
    {
        public string RoutePrefix { get; set; } = "webhooks";
    }

    public class WebhookRoutePrefixConstraint : IRouteConstraint
    {
        public bool Match(HttpContext? httpContext, IRouter route, string routeKey, RouteValueDictionary values,
            RouteDirection routeDirection)
        {
            if (values.TryGetValue("prefix", out var value) && value is string actual)
            {
                var options = (WebhookOptions)httpContext?
                    .RequestServices
                    .GetService(typeof(WebhookOptions));
                // urls are case sensitive
                var expected = options?.RoutePrefix;
                return expected == actual;
            }
            return false;
        }
    }
}