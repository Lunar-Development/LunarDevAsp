﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LunarDev.Migrations
{
    public partial class AddedTwitchfieldstoauthuser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TwitchAuthExpireDate",
                table: "AspNetUsers",
                type: "datetime",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "TwitchAuthorised",
                table: "AspNetUsers",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "TwitchUserAuthCode",
                table: "AspNetUsers",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TwitchAuthExpireDate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TwitchAuthorised",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TwitchUserAuthCode",
                table: "AspNetUsers");
        }
    }
}
