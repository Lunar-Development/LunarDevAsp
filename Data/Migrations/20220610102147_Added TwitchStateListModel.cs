﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LunarDev.Migrations
{
    public partial class AddedTwitchStateListModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TwtichStateList",
                columns: table => new
                {
                    Code = table.Column<string>(type: "varchar(95)", nullable: false)
                        .Annotation("MySql:CharSet", "utf8mb4")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TwtichStateList", x => x.Code);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TwtichStateList");
        }
    }
}
