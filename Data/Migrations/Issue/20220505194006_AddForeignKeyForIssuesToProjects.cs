﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LunarDev.Migrations.Issue
{
    public partial class AddForeignKeyForIssuesToProjects : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Issues_Projects_ProjectsId",
                table: "Issues");

            migrationBuilder.RenameColumn(
                name: "ProjectsId",
                table: "Issues",
                newName: "ProjectId");

            migrationBuilder.RenameIndex(
                name: "IX_Issues_ProjectsId",
                table: "Issues",
                newName: "IX_Issues_ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Issues_Projects_ProjectId",
                table: "Issues",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Issues_Projects_ProjectId",
                table: "Issues");

            migrationBuilder.RenameColumn(
                name: "ProjectId",
                table: "Issues",
                newName: "ProjectsId");

            migrationBuilder.RenameIndex(
                name: "IX_Issues_ProjectId",
                table: "Issues",
                newName: "IX_Issues_ProjectsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Issues_Projects_ProjectsId",
                table: "Issues",
                column: "ProjectsId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
