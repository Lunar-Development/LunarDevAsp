﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LunarDev.Migrations
{
    public partial class ChangedToStatusEnumInIssuesModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InPlanning",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "InProgress",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "InTodo",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "IsCanceled",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "IsSolved",
                table: "Issues");

            migrationBuilder.DropColumn(
                name: "Testing",
                table: "Issues");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Issues",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Issues");

            migrationBuilder.AddColumn<bool>(
                name: "InPlanning",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "InProgress",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "InTodo",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCanceled",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSolved",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Testing",
                table: "Issues",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }
    }
}
